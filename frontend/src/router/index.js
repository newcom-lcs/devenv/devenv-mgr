import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/entornos',
    component: () => import(/* webpackChunkName: "about" */ '../views/Entornos.vue')
  },
  {
    path: '/plantillas',
    component: () => import(/* webpackChunkName: "about" */ '../views/Plantillas.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
