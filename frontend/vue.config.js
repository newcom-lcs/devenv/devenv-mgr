module.exports = {
  "publicPath": "./",
  "devServer": {
    "disableHostCheck": true,
    "port": 8080,
    "https": true,
    "headers": {
      "Access-Control-Allow-Origin": "*"
    },
    "proxy": {
      "/api": {
				target: 'http://' + process.env.API_HOST,
				"secure": false
      }
    }
  },
  "configureWebpack": {
    "plugins": []
  },
  "transpileDependencies": [
    "vuetify"
  ]
}