package api

import (
	// "github.com/go-chi/chi"
	"net/http"
	"github.com/rs/zerolog/log"
	// "dev-env/docker"
	"net/http/httputil"
	"net/url"
	"crypto/tls"
)

func proxy() http.Handler {
	origin, _ := url.Parse("http://10.1.21.83:3101")

	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	
	director := func(req *http.Request) {
		req.Header.Add("X-Forwarded-Host", req.Host)
		req.Header.Add("X-Origin-Host", origin.Host)
		req.URL.Scheme = "http"
		req.URL.Host = origin.Host
		req.URL.Path = "/"
		log.Debug().Msg("Proxy Director")
		log.Trace().Interface("request",req.URL).Msg("")
	}

	proxy := &httputil.ReverseProxy{Director: director}

	return proxy
}

// func proxy() http.Handler {
// 	r := chi.NewRouter()
//   r.Get("/{entorno}/{servicio}", func(w http.ResponseWriter, r *http.Request) {
// 		entorno := chi.URLParam(r, "entorno")
// 		servicio := chi.URLParam(r, "servicio")

// 		envs := docker.GetEnvironmentsMap()
// 		log.Info().Str("Entorno", entorno).Msg("Obteniendo Entorno")

// 		if e, found := envs[entorno]; found {
// 			found = false
// 			for i := range e.Contenedores {
// 				c := e.Contenedores[i]
// 				if c.Servicio == servicio {
// 					log.Trace().Str("Entorno", entorno).Interface("data",c.Puertos).Msg("")
// 					found = true
// 				}
// 			}
// 			if !found {
// 				w.Write([]byte("nope"))
// 				return
// 			}
// 			w.Write([]byte("sipe!"))
// 			return
// 		}

// 		w.Write([]byte("nope"))



//   })
//   return r
// }


// func proxy(w http.ResponseWriter, r *http.Request) {	
// 		w.Write([]byte("welcome"))
// }
