package api

import (
	"dev-env/commands"
	"dev-env/docker"
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/rs/zerolog/log"
	"net/http"
	"os"
	"strings"
)

func routerEntorno() http.Handler {
	r := chi.NewRouter()
	r.Get("/", listaEntornos)
	r.Post("/", createEntorno)
	r.Get("/{entorno}", getEntorno)
	r.Delete("/{entorno}", deleteEntorno)
	r.Put("/{entorno}/stop", stopEntorno)
	r.Put("/{entorno}/start", startEntorno)
	r.Put("/{entorno}/restart", restartEntorno)
	return r
}

func listaEntornos(w http.ResponseWriter, r *http.Request) {
	envs := docker.GetEnvironmentsList()
	js, _ := json.Marshal(envs)
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func getEntorno(w http.ResponseWriter, r *http.Request) {
	entorno := chi.URLParam(r, "entorno")
	envs := docker.GetEnvironmentsMap()
	log.Info().Str("Entorno", entorno).Msg("Obteniendo Entorno")

	if e, found := envs[entorno]; found {
		log.Trace().Str("Entorno", entorno).Interface("data", e).Msg("")
		js, _ := json.Marshal(e)
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
		return
	}

	w.Write([]byte("nope"))

}

func deleteEntorno(w http.ResponseWriter, r *http.Request) {
	entorno := chi.URLParam(r, "entorno")
	envs := docker.GetEnvironmentsMap()

	log.Info().Str("Entorno", entorno).Msg("Eliminando Entorno")
	if e, found := envs[entorno]; found {

		res, err := docker.Down(e.Nombre, true, false, "")
		if err != nil {
			log.Error().Err(err).Interface("data", string(res)).Msg("Error Al intentar Eliminar el Entorno de desarrollo")
			w.Write([]byte(err.Error()))
			return
		}

		w.Write([]byte("Se eliminó el entorno " + entorno))
		return

	}
	w.Write([]byte("nope"))

	return
}

func stopEntorno(w http.ResponseWriter, r *http.Request) {
	entorno := chi.URLParam(r, "entorno")
	envs := docker.GetEnvironmentsMap()

	log.Info().Str("Entorno", entorno).Msg("Deteniendo Entorno")
	if e, found := envs[entorno]; found {

		res, err := docker.Stop(e.Nombre, "")
		if err != nil {
			log.Error().Err(err).Interface("data", string(res)).Msg("Error Al intentar Detener el Entorno de desarrollo")
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		w.Write([]byte("Se detuvo el entorno " + entorno))
		return

	}
	w.Write([]byte("nope"))

	return
}

func startEntorno(w http.ResponseWriter, r *http.Request) {
	entorno := chi.URLParam(r, "entorno")
	envs := docker.GetEnvironmentsMap()

	log.Info().Str("Entorno", entorno).Msg("Iniciando Entorno")
	if e, found := envs[entorno]; found {

		res, err := docker.Start(e.Nombre, "")
		if err != nil {
			log.Error().Err(err).Interface("data", string(res)).Msg("Error Al intentar Iniciar el Entorno de desarrollo")
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		w.Write([]byte("Se inició el entorno " + entorno))
		return

	}
	w.Write([]byte("nope"))

	return
}

func restartEntorno(w http.ResponseWriter, r *http.Request) {
	entorno := chi.URLParam(r, "entorno")
	envs := docker.GetEnvironmentsMap()

	log.Info().Str("Entorno", entorno).Msg("Iniciando Entorno")
	if e, found := envs[entorno]; found {

		res, err := docker.Restart(e.Nombre, "")
		if err != nil {
			log.Error().Err(err).Interface("data", string(res)).Msg("Error Al intentar Reiniciar el Entorno de desarrollo")
			// w.Write([]byte(err.Error()))
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		w.Write([]byte("Se inició el entorno " + entorno))
		return

	}
	w.Write([]byte("nope"))

	return
}

type requestEntorno struct {
	Nombre   string `json:"nombre"`
	Template string `json:"template"`
}

func createEntorno(w http.ResponseWriter, r *http.Request) {

	var e requestEntorno
	err := json.NewDecoder(r.Body).Decode(&e)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var envs []string
	var workingDir string

	if e.Template != "" {
		envs = append(envs, commands.LoadEnvFile(e.Template)...)
		workingDir = commands.GetEnvFileVariable(e.Template, "COMPOSE_DIRECTORY")

		path, err := os.Getwd()
		if err != nil {
			log.Error().Err(err).Msg("Error al obtener variables de entorno")
		}
		pathEnvFile := path + "/" + e.Template
		envs = append(envs, "ENV_FILE="+pathEnvFile)

		if workingDir != "" {
			composeFileVar := commands.GetEnvFileVariable(e.Template, "COMPOSE_FILE")

			composeFileVar = strings.TrimSpace(composeFileVar)
			composeFiles := strings.SplitN(composeFileVar, ":", -1)
			for i, f := range composeFiles {
				composeFiles[i] = workingDir + `/` + f
			}

			envs = append(envs, "COMPOSE_FILE="+strings.Join(composeFiles, ":"))

		}
	}

	res, err := docker.Up(e.Nombre, "", 0, envs, nil, workingDir)
	if err != nil {
		log.Error().Err(err).Interface("data", res).Msg("Error Al intentar Crear el Entorno de desarrollo. Revise el detalle a continuación para determinar la causa")
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	log.Trace().Str("data", string(res)).Msg("")

	w.Write([]byte(res))

	return
}
