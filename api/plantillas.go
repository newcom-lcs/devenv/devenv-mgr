package api

import (
	// "fmt"
	"github.com/go-chi/chi"
	"net/http"
	// "dev-env/docker"
	"encoding/json"
	"path/filepath"
	"dev-env/commands"
	"strings"
	// "log"
	"io/ioutil"
	"os"
)


func routerPlantilla() http.Handler {
  r := chi.NewRouter()
  r.Get("/", listaPlantillas)
  r.Get("/{plantilla}", getPlantilla)
  r.Put("/{plantilla}", createPlantilla)
  r.Delete("/{plantilla}", deletePlantilla)
  return r
}

func listaPlantillas(w http.ResponseWriter, r *http.Request) {	
	matches, _ := filepath.Glob("templates/*")

	js, _ := json.Marshal(matches)
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func getPlantilla(w http.ResponseWriter, r *http.Request) {	
	plantilla := chi.URLParam(r, "plantilla")
	datos := commands.LoadEnvFile(`templates/` + plantilla)
	if datos == nil {
			http.Error(w, "No se pudo abrir plantilla", http.StatusInternalServerError)
	}

	contenido := strings.Join(datos, "\n")

	w.Write([]byte(contenido))
	
}

func createPlantilla(w http.ResponseWriter, r *http.Request) {
	plantilla := chi.URLParam(r, "plantilla")

	responseData,err := ioutil.ReadAll(r.Body)
	if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			// log.Fatal(err)
			return
	}

	f, err := os.Create("templates/" + plantilla)
	if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
	}
	defer f.Close()
	f.Write([]byte(responseData))

	w.Write([]byte("ok"))
	return
}


func deletePlantilla(w http.ResponseWriter, r *http.Request) {	
	plantilla := chi.URLParam(r, "plantilla")
	if err := os.Remove("templates/" + plantilla); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write([]byte("ok"))
}


