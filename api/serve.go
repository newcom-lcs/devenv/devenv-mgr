//Package api ...
package api

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

//Serve Comando Unirse a sesion compartida
type Serve struct {
	Host string `short:"H" long:"host" description:"Host de escucha" default:"0.0.0.0"`
	Port int    `short:"p" long:"puerto" description:"Puerto de Escucha" default:"3100"`
}

//Execute Lista los entornos
func (x *Serve) Execute(args []string) error {

	r := chi.NewRouter()
	r.Use(render.SetContentType(render.ContentTypeJSON))
	// srv := &http.Server{Addr: port}
	

	workDir, _ := os.Getwd()
	filesDir := http.Dir(filepath.Join(workDir, "frontend"))
	FileServer(r, "/", filesDir)

	// r.Get("/", func(w http.ResponseWriter, r *http.Request) {
	//   w.Write([]byte("hi"))
	// })
	
	r.Mount("/api/entornos", routerEntorno())
	r.Mount("/api/plantillas", routerPlantilla())
	r.Mount("/proxy", proxy())
	// r.Mount("/{entorno}", func )

	port := fmt.Sprintf(":%d", x.Port)
	// srv.ListenAndServe(r)
	http.ListenAndServe(port, r)

	return nil
}

// FileServer conveniently sets up a http.FileServer handler to serve
// static files from a http.FileSystem.
func FileServer(r chi.Router, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit any URL parameters.")
	}

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, func(w http.ResponseWriter, r *http.Request) {
		rctx := chi.RouteContext(r.Context())
		pathPrefix := strings.TrimSuffix(rctx.RoutePattern(), "/*")
		fs := http.StripPrefix(pathPrefix, http.FileServer(root))
		fs.ServeHTTP(w, r)
	})
}
