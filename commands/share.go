//Package commands 
package commands

import (
	"fmt"
	"dev-env/docker"
	"errors"
	"strconv"
	"os/exec"
	"os"
)

//Share Comando para compartir los puertos del entorno 
type Share struct {
	Host string 		`short:"H" long:"host" description:"host centralizador de conexiones"`
	UserName string 		`short:"u" long:"username" description:"usuario"`
	// User string 		`short:"u" long:"username" description:"host centralizador de conexiones"`
	Args shareArgs	`positional-args:"yes"`
}

type shareArgs struct {
	Entorno string `description:"Entorno del Proyecto a Compartir" optional:"yes"`
}

//Execute Lista los entornos
func (x *Share) Execute(args []string) error {
	envs := docker.GetEnvironmentsMap()

	env, found := envs[x.Args.Entorno];
	if !found {
		return errors.New("Entorno '" + x.Args.Entorno +"' no encontrado")		
	}
	var puertos []string
	var puertosCompartidos []string
	for k := range env.Contenedores {
		c := env.Contenedores[k]
		for p := range c.Puertos {
				if int(c.Puertos[p].PublicPort) > 0 {
					port := strconv.Itoa(int(c.Puertos[p].PublicPort))
					// puertos = append(puertos, fmt.Sprintf("localhost:%v:%v:%v", port, c.Puertos[p].IP, port))
					puertos = append(puertos, fmt.Sprintf("%v:%v:%v", port, c.Puertos[p].IP, port))
					puertosCompartidos = append(puertosCompartidos, port)					
				}
		}
	}
	// return nil

	cmd := exec.Command("ssh", "-N")
	defer cmd.Process.Kill();

	cmd.Env = append(os.Environ())
	variablesEntorno := LoadEnvFile(".env")
	cmd.Env = append(cmd.Env, variablesEntorno...)

	// busco la variable SHARE_HOST
	for _,l := range variablesEntorno {
		a,b := ParseEnvLine(l)
		if a == "SHARE_HOST" {
			x.Host = b
			break
		}

	}

	cmd.Args = append(cmd.Args, x.Host)

	if(x.UserName != "") {
		cmd.Args = append(cmd.Args, "-l")
		cmd.Args = append(cmd.Args, x.UserName)
	}

	for _, p := range puertos {
		cmd.Args = append(cmd.Args, "-R")
		cmd.Args = append(cmd.Args, p)
	}

	cmd.Args = append(cmd.Args, "-o")
	cmd.Args = append(cmd.Args, "ExitOnForwardFailure=yes")

	fmt.Printf("%v\n", cmd.Args)

	fmt.Println("Inciando tunel")
	fmt.Printf("Puertos Compartidos: %v\n", puertosCompartidos)
	fmt.Println("Presione Control-C para finalizar")


	out, _ := cmd.CombinedOutput()
	
	// if err != nil {
		fmt.Printf("%s", out)
		// return err
	// }

	return nil
}

