//Package commands contiene la definición de los comandos disponibles desde linea de comandos
package commands

import (
	"fmt"
	"dev-env/docker"
)

//Down Comando para la visualización de lgos
type Down struct {
	Force bool 	`short:"f" long:"force" description:"Forzar detención del Entorno" `
	Delete bool 	`short:"d" long:"delete" description:"Elimina los volúmenes asociados al entorno" `
	WorkingDir string 		`short:"w" long:"working-dir" description:"Directorio de trabajo de ejecución de docker-compose"`
	Args downArgs	`positional-args:"yes"`
}

type downArgs struct {
	Entorno string `description:"Nombre del Entorno." required:"yes"`
}

//Execute Muestra el log del entorno especificado
func (x *Down) Execute(args []string) error {
	fmt.Printf("Deteniendo entorno: '%v'\n", x.Args.Entorno)

	// if x.WorkingDir == "" {
	// 	x.WorkingDir = GetEnvFileVariable(x.EnvFile, "COMPOSE_DIRECTORY")
	// }

	res, err := docker.Down(x.Args.Entorno, x.Force, x.Delete, x.WorkingDir)
	if err != nil {
		fmt.Println("Error Al intentar Eliminar el Entorno de desarrollo: ")
		fmt.Printf("%s", res)
		return err
	}
		fmt.Printf("%s", res)

	
	fmt.Printf("Se Eliminó el Entorno: '%v'\n", x.Args.Entorno)

	return nil
}

