//Package commands contiene la definición de los comandos disponibles desde linea de comandos
package commands

import (
	"fmt"
	"dev-env/docker"
	"errors"
	"strconv"
	"strings"
)

//Ps Comando para el listado de entornos 
type Ps struct {
	WorkingDir string 		`short:"w" long:"working-dir" description:"Directorio de trabajo de ejecución de docker-compose"`
	// All bool 	`short:"a" long:"all" description:"Lista los entornos activos e inactivos" `
	Args psArgs	`positional-args:"yes"`
}

type psArgs struct {
	Entorno string `description:"Entorno del Proyecto. Si se especifica, se muestra el estados de los contenedores de dicho proyecto" optional:"yes"`
}

//Execute Lista los entornos
func (x *Ps) Execute(args []string) error {
	envs := docker.GetEnvironmentsMap()


	if x.Args.Entorno != "" {
		fmt.Printf("Detalle de Entorno: '%v'\n", x.Args.Entorno)
		if e, found := envs[x.Args.Entorno]; found {
			fmt.Printf("%-20.20s %-40s %-10s %s\n","Servicio","Contenedor","Estado", "Puertos")
			fmt.Printf("----------------------------------------------------------------------------------------------\n")
			for k := range e.Contenedores {
				c := e.Contenedores[k]
				puertos := ""
				for p := range c.Puertos {
					 if int(c.Puertos[p].PublicPort) > 0 {
						 puertos += c.Puertos[p].IP + ":" + strconv.Itoa(int(c.Puertos[p].PublicPort)) + "->" + strconv.Itoa(int(c.Puertos[p].PrivatePort)) + " "
					 }

				}

				fmt.Printf("%-20.20s %-40.40s %-10.10s %s\n",c.Servicio, c.Nombre, c.State, puertos)
			}
				return nil
		}
		return errors.New("Entorno '" + x.Args.Entorno + "' no encontrado")
	} 

	fmt.Printf("Lista de Entornos\n")
	fmt.Printf("%-20s%-10s%-15s%s\n","Entorno","Estado", "IP Pública", "Puertos")
	fmt.Printf("--------------------------------------------------------------------------------------\n")
	for i, e := range envs {
		// fmt.Printf("%v", e)
		var puertos []string
		ip := ""
		for k := range e.Contenedores {
			c := e.Contenedores[k]
			for p := range c.Puertos {
					if int(c.Puertos[p].PublicPort) > 0 {
						ip = c.Puertos[p].IP
						// puertos = append(puertos,c.Puertos[p].IP + ":" + strconv.Itoa(int(c.Puertos[p].PublicPort)) + "->" + strconv.Itoa(int(c.Puertos[p].PrivatePort)))
						puertos = append(puertos, strconv.Itoa(int(c.Puertos[p].PublicPort)) + "->" + strconv.Itoa(int(c.Puertos[p].PrivatePort)))
					}

			}
		}
		fmt.Printf("%-20s%-10s%-15s%s\n", i, e.Status, ip, strings.Join(puertos, ", "))
	}
	return nil
}

