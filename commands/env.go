//Package commands ...
package commands

import (
	"os"
	"bufio"
	"strings"
	"log"
)

//LoadEnvFile Lee archivo de entorno adicional
func LoadEnvFile(filename string) []string {
	var ret []string
	file, err := os.Open(filename)
    if err != nil {
				log.Println(err)
				return nil
    }
    defer file.Close()

    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
			ret = append(ret, scanner.Text())
        // fmt.Println(scanner.Text())
    }

    if err := scanner.Err(); err != nil {
				log.Println(err)
        return nil
		}
	return ret
}

//ParseEnvLine parsea una línea de archivos env y devuelve la variable y el valor separados
func ParseEnvLine(line string) (key, value string) {
    line = strings.TrimSpace(line)
    fields := strings.SplitN(line, "=", 2)
    if len(fields) > 0 {
        key = strings.TrimSpace(fields[0])
        if len(fields) > 1 {
            value = strings.TrimSpace(fields[1])
        }
    }
    return key, value
}

//GetEnvFileVariable lee una variable de un archivo env y devuelve su valor
func GetEnvFileVariable(filename string, variable string) string {
	vars := LoadEnvFile(filename)
	for _,v := range vars {
		a, b := ParseEnvLine(v)
		if a == variable {
			return b
		}
	}
	return ""
}