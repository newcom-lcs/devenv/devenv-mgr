//Package commands contiene la definición de los comandos disponibles desde linea de comandos
package commands

import (
	"dev-env/docker"
	"fmt"
	"bufio"
)

//Logs Comando para la visualización de lgos
type Logs struct {
	Follow bool 	`short:"f" long:"follow" description:"Muestra el log a medida que va creciendo" `
	Tail int 	`short:"t" long:"tail" description:"mostrar solo las ultimas n lineas" `
	WorkingDir string 		`short:"w" long:"working-dir" description:"Directorio de trabajo de ejecución de docker-compose"`
	Args logsArgs	`positional-args:"yes"`
}

type logsArgs struct {
	Entorno string `description:"Nombre del Entorno." required:"yes"`
}

//Execute Muestra el log del entorno especificado
func (x *Logs) Execute(args []string) error {
	pipe, err := docker.Logs(x.Args.Entorno, x.Follow, x.Tail, x.WorkingDir)
	if err != nil {
		return err
	}

	reader := bufio.NewReader(pipe)
	for  {
		line, _, err := reader.ReadLine()
		if err != nil  {
			break;
		}
		fmt.Printf("%s\n", line)
	}


	return nil


}

