//Package commands contiene la definición de los comandos disponibles desde linea de comandos
package commands

import (
	"dev-env/docker"
	"fmt"
	"bufio"
	// "errors"
)

//Logs Comando para la visualización de lgos
type Compose struct {
	Entorno string
}

type composeArgs struct {
}


//Execute Muestra el log del entorno especificado
func (x *Compose) Execute(args []string) error {

	pipe, err := docker.Compose(x.Entorno, args)
	if err != nil {
		fmt.Printf("%s\n", err)
		return err
	}

	reader := bufio.NewReader(pipe)
	for  {
		line, _, err := reader.ReadLine()
		if err != nil  {
			break;
		}
		fmt.Printf("%s\n", line)
	}


	return nil


}

