//Package commands ...
package commands

import (
	"fmt"
	// "dev-env/docker"
	// "errors"
	// "strconv"
	"os/exec"
	"os"
)

//Join Comando Unirse a sesion compartida 
type Join struct {
	Host string 		`short:"H" long:"host" description:"host centralizador de conexiones"`
	UserName string 		`short:"u" long:"username" description:"usuario"`
	Ports 		[]string 	`short:"p" long:"port" description:"puerto compartido por nuestrx compa" required:"yes"`
}


//Execute Lista los entornos
func (x *Join) Execute(args []string) error {

	cmd := exec.Command("ssh", "-N")
	defer cmd.Process.Kill();

	variablesEntorno := LoadEnvFile(".env")
	cmd.Env = append(cmd.Env, variablesEntorno...)

	// busco la variable SHARE_HOST
	for _,l := range variablesEntorno {
		a,b := ParseEnvLine(l)
		if a == "SHARE_HOST" {
			x.Host = b
			break
		}

	}

	cmd.Args = append(cmd.Args, x.Host)
	cmd.Env = append(os.Environ())
	if(x.UserName != "") {
		cmd.Args = append(cmd.Args, "-l")
		cmd.Args = append(cmd.Args, x.UserName)
	}
	for _, p := range x.Ports {
		cmd.Args = append(cmd.Args, "-L")
		cmd.Args = append(cmd.Args, fmt.Sprintf("0.0.0.0:%s:localhost:%s", p, p))
	}

	cmd.Args = append(cmd.Args, "-o")
	cmd.Args = append(cmd.Args, "ExitOnForwardFailure=yes")

	
	fmt.Printf("%v\n", cmd.Args)
	
	// 	cmd.Args = append(cmd.Args, "echo hola")


	fmt.Println("Inciando tunel")
	fmt.Printf("Se redireccionan los siguientes puertos locales: %v\n", x.Ports)
	fmt.Println("presione Control-C para finalizar")

	out, err := cmd.CombinedOutput()
	
	if err != nil {
		fmt.Printf("%s", out)
		return err
	}

	return nil
}

