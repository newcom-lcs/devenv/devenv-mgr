//Package commands contiene la definición de los comandos disponibles desde linea de comandos
package commands

import (
	"fmt"
	"dev-env/docker"
)

//Restart Comando para la visualización de lgos
type Restart struct {
	WorkingDir string 		`short:"w" long:"working-dir" description:"Directorio de trabajo de ejecución de docker-compose"`
	Args restartArgs	`positional-args:"yes"`
}

type restartArgs struct {
	Entorno string `description:"Nombre del Entorno." required:"yes"`
	Servicio string `description:"Nombre del Servicio a Reiniciar." `
}

//Execute Muestra el log del entorno especificado
func (x *Restart) Execute(args []string) error {
	if x.Args.Servicio == "" {
		fmt.Printf("Reiniciando entorno: '%v'\n", x.Args.Entorno)
	} else {
		fmt.Printf("Reiniciando servicio: '%v'\n", x.Args.Servicio)
	}


	// if x.WorkingDir == "" {
	// 	x.WorkingDir = GetEnvFileVariable(x.EnvFile, "COMPOSE_DIRECTORY")
	// }

	res, err := docker.Restart(x.Args.Entorno, x.Args.Servicio)
	if err != nil {
		fmt.Println("Error Al intentar Reiniciar el Entorno de desarrollo: ")
		fmt.Printf("%s", res)
		return err
	}

	fmt.Printf("%s", res)

	
	fmt.Println("Se Reinició el Entorno/Servicio")

	return nil
}

