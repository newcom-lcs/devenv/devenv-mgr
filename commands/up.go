//Package commands contiene la definición de los comandos disponibles desde linea de comandos
package commands

import (
	"log"
	"os"
	"errors"
	"regexp"
	"dev-env/docker"
	"strings"
	"fmt"
)

//Up Comando para la Inicialización del Entorno de Desarrollo
type Up struct {
	Port 		uint 		`short:"p" long:"port" description:"Puerto Base para el entorno. Se asignarán 5 puertos consecutivos"`
	Envs 		[]string 	`short:"e" long:"environment" description:"Variables de entorno en formato variable=valor" `
	EnvFile string 		`short:"E" long:"env-file" description:"Archivo adicinal de variables de entorno (util para plantillas)"`
	GitURL 		gitURL 		`short:"g" long:"git-url" description:"URL del repositorio Git (https)"  `
	ComposeFiles 	[]string 	`short:"f" long:"compose-file" description:"archivo yalm de docker-compose " `
	Args 		argumentos	`positional-args:"yes"`
	WorkingDir string 		`short:"w" long:"working-dir" description:"Directorio de trabajo de ejecución de docker-compose"`

}

type argumentos struct {
	Nombre string `description:"Nombre del Proyecto" required:"yes"`
}

//Execute Inicia el Entorno de Desarrollo
func (x *Up) Execute(args []string) error {
	log.Printf("Iniciando Entorno de Desarrollo '%s'...\n", x.Args.Nombre)

	var envs []string

	
	if x.EnvFile != "" {
		envs = append(envs, LoadEnvFile(x.EnvFile)...)
		if x.WorkingDir == "" {
			x.WorkingDir = GetEnvFileVariable(x.EnvFile, "COMPOSE_DIRECTORY")			
		}
		path, err := os.Getwd()
		if err != nil {
				log.Println(err)
		}
		pathEnvFile := path + "/" + x.EnvFile
		envs = append(envs, "ENV_FILE=" + pathEnvFile)

	}
	
	envs = append(envs, x.Envs...)

	//tengo que modificar el la variable de entorno
	if x.WorkingDir != "" {
		composeFileVar := GetEnvFileVariable(x.EnvFile, "COMPOSE_FILE")

    composeFileVar = strings.TrimSpace(composeFileVar)
		composeFiles := strings.SplitN(composeFileVar, ":", -1)
		for i, f := range composeFiles {
			composeFiles[i] =  x.WorkingDir + `/` + f
		}

		envs = append(envs, "COMPOSE_FILE=" + strings.Join(composeFiles, ":"))

	}
	


	res, err := docker.Up(x.Args.Nombre, x.GitURL.String(), x.Port, envs, x.ComposeFiles, x.WorkingDir)
	if err != nil {
		fmt.Println("Error Al intentar Crear el Entorno de desarrollo. Revise el detalle a continuación para determinar la causa")
		fmt.Printf("%s", res)
		return err
	}
	fmt.Printf("%s", res)

	return nil
}

type gitURL string

func (c *gitURL) String() string {
	return string(*c)
}

func (c *gitURL) UnmarshalFlag(s string) error {
	re := regexp.MustCompile(`(https).+.git$`)
	if re.Find([]byte(s)) == nil {
		return errors.New("URL de repositorio inválido\n" +
											"Formato válidos:\n" +
											"\t https://<url>.git\n" )
	}

	*c = gitURL(s)
	return nil
}

func (c *gitURL) MarshalFlag() (string, error) {
	return string(*c), nil
}


func (c *gitURL) IsValidValue(s string) error {
	re := regexp.MustCompile(`(git@|https).+.git$`)
	if re.Find([]byte(s)) == nil {
		return errors.New("URL de repositorio inválido\n" +
											"Formatos válidos:\n" +
											"\t https://<url>.git\n" + 
											"\t git@<url>.git\n")
	}
	return nil
}


type repoPath string

func (c *repoPath) String() string {
	return string(*c)
}

func (c *repoPath) UnmarshalFlag(s string) error {
	re := regexp.MustCompile(`^[/].+`)
	if re.Find([]byte(s)) == nil {
		return errors.New("El Path debe se absoluto" )
	}

	*c = repoPath(s)
	return nil
}

func (c *repoPath) MarshalFlag() (string, error) {
	return string(*c), nil
}
