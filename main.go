package main

import (
	"strconv"
	"os"
	"github.com/jessevdk/go-flags"
	"dev-env/commands"
	"dev-env/api"
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log")


type options struct {
	/*
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`
	*/
}


func main() {
	// opts := options{}
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	logLevel,err := strconv.Atoi(os.Getenv("LOG_LEVEL"))
	if err!=nil {
		logLevel = -1
	}	
	zerologLevel := zerolog.Level(logLevel)
	zerolog.SetGlobalLevel(zerologLevel)
	log.Logger = log.With().Caller().Logger()
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	var parser = flags.NewParser(nil, flags.Default)

	var upCmd commands.Up
	var psCmd commands.Ps
	var logCmd commands.Logs
	var downCmd commands.Down
	var composeCmd commands.Compose
	var shareCmd commands.Share
	var joinCmd commands.Join
	var restartCmd commands.Restart
	var serveCmd api.Serve


	if len(os.Args) > 2 && os.Args[1] == "compose" {
		// fmt.Printf("compose!!")

		if len(os.Args) < 4 {
			fmt.Printf("Uso %v compose <entorno> <comando> [parametros]\n", os.Args[0] )
			return
		}
		composeCmd.Entorno = os.Args[2]
		command := os.Args[3]
		if command == "up" {
			fmt.Printf("Para crear un entorno nuevo utilice %v up \n", os.Args[0] )
			return
		}
		args := os.Args[3:]
		composeCmd.Execute(args)
		return
	}


	parser.AddCommand("up",
		"Inicar nuevo Entorno",
		"Este comando es utilizado para iniciar un nuevo entorno de desarrollo.",
		&upCmd)

	parser.AddCommand("ps",
		"Listar Entornos de Desarrollo",
		"Este comando es utilizado para listar entornos de desarrollos activos e inactivos.",
		&psCmd)

	parser.AddCommand("logs",
		"Visualización de logs",
		"Este comando muestra los logs generados por el entorno especificado.",
		&logCmd)

	parser.AddCommand("down",
		"Detiene un Entorno",
		"Este comando detiene/elimina un entorno de desarrollo.",
		&downCmd)

	parser.AddCommand("restart",
		"Reinicia un Entorno / Servicio",
		"Reinicia un Entorno / Servicio.",
		&restartCmd)


	parser.AddCommand("share",
		"Comparte entorno via ssh",
		"Inicia un tunel ssh con un concentrador para compartir los puertos del entorno.",
		&shareCmd)

	parser.AddCommand("join",
		"Se conecta a un entorno compartido",
		"Inicia un tunel ssh con un concentrador para conectarse con un entorno compartido.",
		&joinCmd)

	parser.AddCommand("serve",
		"Inicia Frontend WEB para dev-env",
		"Inicia Frontend WEB para dev-env.",
		&serveCmd)


	if _, err := parser.Parse(); err != nil {
		os.Exit(1)
		}


}

