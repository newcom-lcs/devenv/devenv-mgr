# dev-env

Entorno virtual para desarrollo, utiliza en docker y docker-compose

## Características
* 3 Entornos soportados:
	* theia: Entorno theia con node
	* theia-go: Entorno theia preparado para desarrollo en go
	* theia-dind: Imagen con theia, docker y docker-compose!


## Uso
1. crear archivo .env: `cp example.env .env`
2. configurar archivo de entorno 
3. iniciar entorno: `docker-compose up -d`
