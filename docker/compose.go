//Package docker ...
package docker

import (
	// "log"
	// "github.com/rs/zerolog"
	"bufio"
	"errors"
	"fmt"
	"github.com/rs/zerolog/log"
	"io"
	"os"
	"os/exec"
	"strconv"
	// "os/user"
	"strings"
)

//Up Ejecuta el comando docker-compose up
func Up(name string, url string, port uint, envs []string, composeFiles []string, workingDir string) ([]byte, error) {
	log.Info().Interface("entorno", name).Msg("Iniciando Entorno")

	env := append(os.Environ())

	entornos := GetEnvironmentsMap()
	_, found := entornos[name]
	if found {
		return Restart(name, "")
	}

	// log.Debug().Msg("Actualizando imágenes")
	// pull := exec.Command("docker-compose", "pull")
	// err := pull.Run()
	// if(err != nil) {
	// 	log.Error().Err(err).Msg("error al ejecutar docker-compose pull")
	// }

	log.Debug().Msg("Ejecutando docker-compose up -d")
	// cmd := exec.Command("docker-compose", "up", "-d")
	cmd := exec.Command("/bin/sh", "-c", "docker-compose pull && docker-compose up -d")

	// cargo las variables de entorno
	env = append(os.Environ())
	env = append(env, envs...)

	// if workingDir != "" {
	// 	cmd.Dir = workingDir
	// }

	// cargo el nombre del proyecto
	env = append(env,
		"COMPOSE_PROJECT_NAME="+name,
		"PROJECT_NAME="+name,
	)

	//cargar url git
	if url != "" {
		env = append(env,
			"GIT_REPO="+url,
		)
	}

	// if port > 0 {
	basePortStr := os.Getenv("BASE_PORT")
	var basePort uint64
	basePort, _ = strconv.ParseUint(basePortStr, 10, 32)

	log.Debug().Interface("BASE_PORT", basePort).Msg("")

	if basePort > 0 {
		puertosActivos := GetPublicActivePorts()
		//busco 3 puertoslibres
		for i := 1; i <= 3; i++ {
			encontrado := false
			for !encontrado {
				log.Debug().Interface("port", basePort).Msg("Chequeando disponibilidad de puerto")
				ocupado := false
				for _, p := range puertosActivos {
					if p == uint16(basePort) {
						ocupado = true
						break
					}
				}
				if !ocupado {
					env = append(env,
						"ENV_PORT"+strconv.Itoa(int(i))+"="+strconv.Itoa(int(basePort)),
					)
					encontrado = true
				}
				basePort++
			}
		}
	}

	// if(len(composeFiles) > 0) {
	// cmd.Env = append(cmd.Env, "COMPOSE_FILE=" + strings.Join(composeFiles,":"))
	// }

	cmd.Env = env
	log.Trace().Interface("Env", env).Msg("Cargando variables de entorno")

	out, err := cmd.CombinedOutput()
	if err != nil {
		return out, err
	}
	return out, nil

}

//Down Ejecuta el comando docker-compose down
func Down(name string, force bool, delete bool, workingDir string) ([]byte, error) {

	entornos := GetEnvironmentsMap()
	e, found := entornos[name]
	if !found {
		return nil, errors.New("Entorno '" + name + "' no encontrado")
	}

	if e.Status == "running" && !force {
		return nil, errors.New("El entorno '" + name + "' está activo, utilice -f para forzar")
	}

	var cmd *exec.Cmd

	if delete {
		cmd = exec.Command("docker-compose", "down", "-v", "--remove-orphans")
	} else {
		cmd = exec.Command("docker-compose", "down", "--remove-orphans")
	}
	composeFile := strings.Replace(e.ComposeFiles, ",", ":", -1)

	cmd.Env = append(os.Environ(),
		"PROJECT_NAME="+name,
		"COMPOSE_PROJECT_NAME="+name,
		"COMPOSE_FILE="+composeFile,
		"NETWORK_NAME="+e.Network,
	)

	if workingDir != "" {
		cmd.Dir = workingDir
	}

	out, err := cmd.CombinedOutput()
	if err != nil {
		return out, err
	}
	return out, nil
}

//Stop Ejecuta el comando docker-compose stop
func Stop(name string, workingDir string) ([]byte, error) {

	entornos := GetEnvironmentsMap()
	e, found := entornos[name]
	if !found {
		return nil, errors.New("Entorno '" + name + "' no encontrado")
	}

	var cmd *exec.Cmd

	cmd = exec.Command("docker-compose", "stop")
	composeFile := strings.Replace(e.ComposeFiles, ",", ":", -1)

	cmd.Env = append(os.Environ(),
		"PROJECT_NAME="+name,
		"COMPOSE_PROJECT_NAME="+name,
		"COMPOSE_FILE="+composeFile,
		"NETWORK_NAME="+e.Network,
	)

	if workingDir != "" {
		cmd.Dir = workingDir
	}

	out, err := cmd.CombinedOutput()
	if err != nil {
		return out, err
	}
	return out, nil
}

//Start Ejecuta el comando docker-compose start
func Start(name string, workingDir string) ([]byte, error) {

	entornos := GetEnvironmentsMap()
	e, found := entornos[name]
	if !found {
		return nil, errors.New("Entorno '" + name + "' no encontrado")
	}

	var cmd *exec.Cmd

	cmd = exec.Command("docker-compose", "start")
	composeFile := strings.Replace(e.ComposeFiles, ",", ":", -1)

	cmd.Env = append(os.Environ(),
		"PROJECT_NAME="+name,
		"COMPOSE_PROJECT_NAME="+name,
		"COMPOSE_FILE="+composeFile,
		"NETWORK_NAME="+e.Network,
	)

	if workingDir != "" {
		cmd.Dir = workingDir
	}

	out, err := cmd.CombinedOutput()
	if err != nil {
		return out, err
	}
	return out, nil
}

//Logs Logs el comando docker-compose logs
func Logs(name string, follow bool, tail int, workingDir string) (*io.PipeReader, error) {

	entornos := GetEnvironmentsMap()
	e, found := entornos[name]
	if !found {
		return nil, errors.New("Entorno '" + name + "' no encontrado")
	}

	var cmd *exec.Cmd
	cmd = exec.Command("docker-compose", "logs")
	if follow {
		cmd.Args = append(cmd.Args, "-f")
	}
	if tail > 0 {
		cmd.Args = append(cmd.Args, fmt.Sprintf("--tail=%v", tail))
		// cmd = append(cmd, "--tail " + tail)
	}

	composeFile := strings.Replace(e.ComposeFiles, ",", ":", -1)

	cmd.Env = append(os.Environ(),
		"PROJECT_NAME="+name,
		"COMPOSE_PROJECT_NAME="+name,
		"COMPOSE_FILE="+composeFile,
		"NETWORK_NAME="+e.Network,
	)

	// fmt.Printf("%v", cmd.Env)

	stdout, _ := cmd.StdoutPipe()
	stderr, _ := cmd.StderrPipe()

	if workingDir != "" {
		cmd.Dir = workingDir
	}

	if err := cmd.Start(); err != nil {
		return nil, err
	}

	pipeReader, pipeWriter := io.Pipe()

	go func() {
		reader := bufio.NewReader(stdout)
		for {
			line, _, err := reader.ReadLine()
			if err != nil {
				break
			}
			fmt.Fprintf(pipeWriter, "%s\n", line)
		}

		reader = bufio.NewReader(stderr)

		for {
			line, _, err := reader.ReadLine()
			if err != nil {
				break
			}
			fmt.Fprintf(pipeWriter, "!! %s\n", line)
		}

		cmd.Wait()

		pipeWriter.Close()
	}()

	return pipeReader, nil
}

//Compose Logs el comando docker-compose logs
func Compose(name string, args []string) (*io.PipeReader, error) {

	entornos := GetEnvironmentsMap()
	e, found := entornos[name]
	if !found {
		return nil, errors.New("Entorno '" + name + "' no encontrado")
	}

	var cmd *exec.Cmd
	cmd = exec.Command("docker-compose", args...)

	composeFile := strings.Replace(e.ComposeFiles, ",", ":", -1)

	cmd.Env = append(os.Environ(),
		"PROJECT_NAME="+name,
		"COMPOSE_PROJECT_NAME="+name,
		"COMPOSE_FILE="+composeFile,
		"NETWORK_NAME="+e.Network,
	)
	stdout, _ := cmd.StdoutPipe()
	stderr, _ := cmd.StderrPipe()

	if err := cmd.Start(); err != nil {
		return nil, err
	}

	pipeReader, pipeWriter := io.Pipe()

	go func() {
		reader := bufio.NewReader(stdout)
		for {
			line, _, err := reader.ReadLine()
			if err != nil {
				break
			}
			fmt.Fprintf(pipeWriter, "%s\n", line)
		}

		reader = bufio.NewReader(stderr)

		for {
			line, _, err := reader.ReadLine()
			if err != nil {
				break
			}
			fmt.Fprintf(pipeWriter, "!! %s\n", line)
		}

		cmd.Wait()

		pipeWriter.Close()
	}()

	return pipeReader, nil
}

//Restart reinicia un contenedor
func Restart(name string, servicio string) ([]byte, error) {

	entornos := GetEnvironmentsMap()
	e, found := entornos[name]
	if !found {
		return nil, errors.New("Entorno '" + name + "' no encontrado")
	}

	var cmd *exec.Cmd

	if servicio != "" {
		cmd = exec.Command("docker-compose", "restart", servicio)
	} else {
		cmd = exec.Command("docker-compose", "restart")
	}
	composeFile := strings.Replace(e.ComposeFiles, ",", ":", -1)

	cmd.Env = append(os.Environ(),
		"PROJECT_NAME="+name,
		"COMPOSE_PROJECT_NAME="+name,
		"COMPOSE_FILE="+composeFile,
		"NETWORK_NAME="+e.Network,
	)

	// if workingDir != "" {
	// 	cmd.Dir = workingDir
	// }

	out, err := cmd.CombinedOutput()
	if err != nil {
		return out, err
	}
	return out, nil
}
