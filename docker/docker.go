//Package docker paquete con métodos comunes para docker
package docker

import (
	"context"
	"github.com/rs/zerolog/log"

	// "sort"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	// "fmt"
)

//Entorno contiene información sobre el entorno de desarrollo
type Entorno struct {
	Nombre             string
	ComposeProjectName string
	ComposeFiles       string
	Status             string
	Contenedores       []Contenedor
	Network            string
}

//Contenedor contiene la información de un contenedor
type Contenedor struct {
	Servicio string
	Nombre   string
	State    string
	Status   string
	Imagen   string
	Puertos  []types.Port
}

//GetEnvironments devuelve un map con los entornos
func GetEnvironmentsMap() map[string]*Entorno {
	// var entornos  map[string]Entorno
	// var entornosCompose map[string]Entorno

	entornos := make(map[string]*Entorno)
	// entornosCompose := make(map[string]*Entorno)

	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		panic(err)
	}

	opts := types.ContainerListOptions{}
	opts.All = true
	containers, err := cli.ContainerList(ctx, opts)
	if err != nil {
		panic(err)
	}

	//Obtener la lista de entornos
	for _, container := range containers {
		isEnv := container.Labels["com.newcom-lcs.devenv.isEnv"]
		// service := container.Labels["com.docker.compose.service"]
		composeProject := container.Labels["com.docker.compose.project"]
		if composeProject == "" || entornos[composeProject] != nil || isEnv != "true" {
			continue
		}
		// fmt.Printf("%#v\n\n", container)

		status := container.State
		var entorno Entorno
		entorno = Entorno{
			Status:             status,
			Nombre:             composeProject,
			ComposeProjectName: composeProject,
			ComposeFiles:       container.Labels["com.docker.compose.project.config_files"],
			Network:            container.HostConfig.NetworkMode,
		}
		entornos[composeProject] = &entorno
		// entornosCompose[composeProject] = &entorno
	}

	//Obtener la lista de contenedores de cada entorno
	for _, container := range containers {
		composeProject := container.Labels["com.docker.compose.project"]

		if c, found := entornos[composeProject]; found {
			contenedor := Contenedor{
				State:    container.State,
				Status:   container.Status,
				Nombre:   container.Names[0],
				Imagen:   container.Image,
				Puertos:  container.Ports,
				Servicio: container.Labels["com.docker.compose.service"],
			}
			// fmt.Printf("%#v\n", container.Ports)
			c.Contenedores = append(c.Contenedores, contenedor)
		}

	}

	return entornos
}

//GetEnvironmentsList devuelve la lista de entornos en formato de slice
func GetEnvironmentsList() []*Entorno {
	var list []*Entorno
	envs := GetEnvironmentsMap()
	for _, e := range envs {
		list = append(list, e)
	}
	return list
}

//GetPublicActivePorts devuelve la lista de puertos públicos utilizados por contenedores docker
func GetPublicActivePorts() []uint16 {
	log.Info().Msg("Obteniendo lista de puertos en uso")
	var list []uint16
	envs := GetEnvironmentsMap()
	for _, e := range envs {
		for _, c := range e.Contenedores {
			for _, p := range c.Puertos {
				log.Trace().Interface("puerto", p.PublicPort).Msg("Puerto Activo")
				list = append(list, p.PublicPort)

				// sort.Ints(list)
			}

		}
		// list = append(list, e)
	}
	return list
}
