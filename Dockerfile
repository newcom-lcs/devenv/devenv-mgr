FROM node:alpine as ui
WORKDIR "/app"
ADD frontend /app
RUN yarn install && yarn lint && yarn build

FROM golang:alpine as golang
ADD . /go/src/app
WORKDIR /go/src/app
RUN go mod vendor && go build && go install && ls -l /go/bin


FROM docker/compose
RUN apk add --no-cache docker
WORKDIR "/app"
ADD entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
ADD compose-files /app/compose-files
COPY --from=ui /app/dist /app/frontend
COPY --from=golang /go/bin/* /app/app
